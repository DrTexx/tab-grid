const path = require("path")

module.exports = {
  entry: {
    background: "./src/background.js",
    grid: "./src/grid.js",
    popup: "./src/popup.js"
  },
  output: {
    path: path.resolve(__dirname, "addon/scripts-dist"),
    filename: "[name].js"
  },
  mode: 'none',
}
