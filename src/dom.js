import * as sessionCaptureStore from "./session-capture-store"
import { getRecentlyClosedTabs } from "./browser"

let reloadImageUri = browser.runtime.getURL("assets/images/reload.svg")
let tabCloseImageUri = browser.runtime.getURL("assets/images/circle-close-red.svg")
let questionMarkImageUri = browser.runtime.getURL("assets/images/question-mark.svg")

function _setTabImage(tabId, imageUri) {
  if (!imageUri) { console.error(`Failed to get an image for this tab!: ${tabId}`) }
  let tabPreview = document.getElementById(`tab-preview-${tabId}`)
  tabPreview.src = imageUri
}

export function updateTabPreview(tab, tabPreview) {
  // if the tab is 'discarded' (unloaded) (therefore capture won't work)
  if (tab.discarded || tabPreview.classList.contains('is-reloading')) {
    tabPreview.classList.add('discarded')
    _setTabImage(tab.id, reloadImageUri)
  } else {
    // recapture tab preview
    browser.tabs.captureTab(tab.id)
      .then(
        (imageUri) => { _setTabImage(tab.id, imageUri) },
        (err) => { console.error(err) }
      )
  }
  tabPreview.title = tab.title // update tab title
}

function updateTabPreviews(reloadDiscarded = false) {
  let tabPreviews = document.getElementsByClassName('tab-preview')

  for (let tabPreview of tabPreviews) {
    let tabId = Number(tabPreview.getAttribute('tabid'))
    browser.tabs.get(tabId).then((tab) => {
      if (reloadDiscarded && tab.discarded) {
        browser.tabs.reload(tabId)
        tabPreview.classList.add('is-reloading')
      }
      updateTabPreview(tab, tabPreview)
    })
  }
}

function createTabPreview(tab) {
  // create tab preview frame element
  let tabPreviewFrame = document.createElement('div')
  tabPreviewFrame.classList.add('tab-preview-frame')

  // create tab preview element
  let tabPreview = document.createElement('img')

  tabPreview.setAttribute('height', '100%')
  tabPreview.setAttribute('width', "100%")
  tabPreview.id = `tab-preview-${tab.id}`
  // tabPreview.alt = tab.id
  tabPreview.title = tab.title // show tab title tooltip on hover
  tabPreview.setAttribute('tabid', tab.id)
  tabPreview.classList.add('tab-preview')

  // create tab close element
  let tabClose = document.createElement('img')

  tabClose.classList.add('tab-close')
  tabClose.src = tabCloseImageUri
  tabClose.setAttribute('tabid', tab.id)

  tabPreviewFrame.appendChild(tabPreview)
  tabPreviewFrame.appendChild(tabClose)

  return tabPreviewFrame
}

export function createTabGridEntry(tab) {
  let tabGridEntry = document.createElement('div')
  tabGridEntry.classList.add('tab-grid-entry')
  tabGridEntry.id = `tag-grid-entry-${tab.id}`

  let tabPreview = createTabPreview(tab)
  tabGridEntry.appendChild(tabPreview)

  return tabGridEntry
}

export function createSessionEntry(session) {
  let sessionEntry = document.createElement('img')

  sessionEntry.title = session.tab.title
  sessionEntry.src = sessionCaptureStore.getImageUri(session.tab.sessionId) || questionMarkImageUri
  sessionEntry.id = `session-entry-${session.tab.sessionId}`
  sessionEntry.classList.add('session-entry')
  sessionEntry.setAttribute('sessionid', session.tab.sessionId)
  return sessionEntry
}

export function createRecentlyClosed() {
  getRecentlyClosedTabs()
    .then((sessions) => {
      let recentlyClosed = document.getElementById('recently-closed')
      let currentSessions = document.createDocumentFragment()

      sessions.forEach((session) => {
        let sessionEntry = createSessionEntry(session)

        currentSessions.appendChild(sessionEntry)
      })

      recentlyClosed.appendChild(currentSessions)
    })
}

export function createTabGrid() {
  browser.tabs.query({ currentWindow: true })
    .then((tabs) => {
      let tabGrid = document.getElementById('tab-grid')
      let currentTabs = document.createDocumentFragment()
      let limit = 20
      let counter = 0

      tabGrid.textContent = ''

      for (let tab of tabs) {
        if (!tab.active && counter <= limit) {
          let tabGridEntry = createTabGridEntry(tab)

          currentTabs.appendChild(tabGridEntry)
        }

        counter += 1
      }

      tabGrid.appendChild(currentTabs)

      updateTabPreviews()
    })
}
