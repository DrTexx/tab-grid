// Zoom constants. Define Max, Min, increment and default values
const ZOOM_INCREMENT = 0.2
const MAX_ZOOM = 5
const MIN_ZOOM = 0.3
const DEFAULT_ZOOM = 1

function firstUnpinnedTab(tabs) {
  for (let tab of tabs) {
    if (!tab.pinned) {
      return tab.index
    }
  }
}

function _setTabImage(tabId, imageUri) {
  if (!imageUri) { console.error(`Failed to get an image for this tab!: ${tabId}`) }
  let tabPreview = document.getElementById(`tab-preview-${tabId}`)
  tabPreview.src = imageUri
}

function createTabLink(tab) {
  let tabLink = document.createElement('a')

  tabLink.textContent = tab.title || tab.id
  tabLink.setAttribute('href', tab.id)
  tabLink.classList.add('switch-tabs')

  return tabLink
}

function createTabPreview(tab) {
  // create tab preview frame element
  let tabPreviewFrame = document.createElement('div')
  tabPreviewFrame.classList.add('tab-preview-frame')

  // create tab preview element
  let tabPreview = document.createElement('img')

  tabPreview.setAttribute('height', '100%')
  tabPreview.setAttribute('width', "100%")
  tabPreview.id = `tab-preview-${tab.id}`
  tabPreview.alt = tab.id
  tabPreview.setAttribute('tabid', tab.id)
  tabPreview.classList.add('tab-preview')

  tabPreviewFrame.appendChild(tabPreview)

  return tabPreviewFrame
}

function createTabListItem(tab) {
  let tabListItem = document.createElement('div')

  let tabLink = createTabLink(tab)
  tabListItem.appendChild(tabLink)

  let tabPreview = createTabPreview(tab)
  tabListItem.appendChild(tabPreview)

  return tabListItem
}

function updateTabPreview(tab, tabPreview) {
  // if the tab is 'discarded' (unloaded) (therefore capture won't work)
  if (tab.discarded) {
    tabPreview.classList.add('discarded')
    let imageUri = browser.runtime.getURL("assets/images/reload.svg")
    _setTabImage(tab.id, imageUri)
  } else {
    // recapture tab preview
    browser.tabs.captureTab(tab.id)
      .then(
        (imageUri) => { _setTabImage(tab.id, imageUri) },
        (err) => { console.error(err) }
      )
  }
}

function updateTabPreviews() {
  let tabPreviews = document.getElementsByClassName('tab-preview')

  for (let tabPreview of tabPreviews) {
    let tabId = Number(tabPreview.getAttribute('tabid'))
    browser.tabs.get(tabId).then((tab) => {
      updateTabPreview(tab, tabPreview)
    })
  }
}

/**
 * listTabs to switch to
 */
function listTabs() {
  getCurrentWindowTabs().then((tabs) => {
    let tabsList = document.getElementById('tabs-list')
    let currentTabs = document.createDocumentFragment()
    let limit = 5
    let counter = 0

    tabsList.textContent = ''

    for (let tab of tabs) {
      if (!tab.active && counter <= limit) {
        let tabListItem = createTabListItem(tab)
        currentTabs.appendChild(tabListItem)
      }

      counter += 1
    }

    tabsList.appendChild(currentTabs)

    updateTabPreviews()
  })
}

document.addEventListener("DOMContentLoaded", listTabs)

function getCurrentWindowTabs() {
  return browser.tabs.query({ currentWindow: true })
}

document.addEventListener("click", (e) => {
  function callOnActiveTab(callback) {
    getCurrentWindowTabs().then((tabs) => {
      for (let tab of tabs) {
        if (tab.active) {
          callback(tab, tabs)
        }
      }
    })
  }

  if (e.target.id === "tabs-grid") {
    let creating = browser.tabs.create({ url: 'grid.html' })

    creating.then(
      () => { console.log("created!"); window.close() },
      () => { console.log("failed to create!") }
    )
  }

  if (e.target.id === "tabs-capture") {
    callOnActiveTab((tab) => {
      let capturing = browser.tabs.captureTab(
        tab.id               // optional integer
        // options              // optional extensionTypes.ImageDetails
      )

      function onCaptured(imageUri) {
        console.log(imageUri)
        document.getElementById("tabs-capture-img").src = imageUri
      }

      function onError(error) {
        console.log(`Error: ${error}`)
      }

      capturing.then(onCaptured, onError)
    })
  }

  if (e.target.id === "tabs-move-beginning") {
    callOnActiveTab((tab, tabs) => {
      let index = 0
      if (!tab.pinned) {
        index = firstUnpinnedTab(tabs)
      }
      console.log(`moving ${tab.id} to ${index}`)
      browser.tabs.move([tab.id], { index })
    })
  }

  if (e.target.id === "tabs-move-end") {
    callOnActiveTab((tab, tabs) => {
      let index = -1
      if (tab.pinned) {
        let lastPinnedTab = Math.max(0, firstUnpinnedTab(tabs) - 1)
        index = lastPinnedTab
      }
      browser.tabs.move([tab.id], { index })
    })
  }

  else if (e.target.id === "tabs-duplicate") {
    callOnActiveTab((tab) => {
      browser.tabs.duplicate(tab.id)
    })
  }

  else if (e.target.id === "tabs-reload") {
    callOnActiveTab((tab) => {
      browser.tabs.reload(tab.id)
    })
  }

  else if (e.target.id === "tabs-remove") {
    callOnActiveTab((tab) => {
      browser.tabs.remove(tab.id)
    })
  }

  else if (e.target.id === "tabs-create") {
    browser.tabs.create({ url: "https://developer.mozilla.org/en-US/Add-ons/WebExtensions" })
  }

  else if (e.target.id === "tabs-create-reader") {
    browser.tabs.create({ url: "https://developer.mozilla.org/en-US/Add-ons/WebExtensions", openInReaderMode: true })
  }

  else if (e.target.id === "tabs-alertinfo") {
    callOnActiveTab((tab) => {
      let props = ""
      for (let item in tab) {
        props += `${item} = ${tab[item]} \n`
      }
      alert(props)
    })
  }

  else if (e.target.id === "tabs-add-zoom") {
    callOnActiveTab((tab) => {
      let gettingZoom = browser.tabs.getZoom(tab.id)
      gettingZoom.then((zoomFactor) => {
        //the maximum zoomFactor is 5, it can't go higher
        if (zoomFactor >= MAX_ZOOM) {
          alert("Tab zoom factor is already at max!")
        } else {
          let newZoomFactor = zoomFactor + ZOOM_INCREMENT
          //if the newZoomFactor is set to higher than the max accepted
          //it won't change, and will never alert that it's at maximum
          newZoomFactor = newZoomFactor > MAX_ZOOM ? MAX_ZOOM : newZoomFactor
          browser.tabs.setZoom(tab.id, newZoomFactor)
        }
      })
    })
  }

  else if (e.target.id === "tabs-decrease-zoom") {
    callOnActiveTab((tab) => {
      let gettingZoom = browser.tabs.getZoom(tab.id)
      gettingZoom.then((zoomFactor) => {
        //the minimum zoomFactor is 0.3, it can't go lower
        if (zoomFactor <= MIN_ZOOM) {
          alert("Tab zoom factor is already at minimum!")
        } else {
          let newZoomFactor = zoomFactor - ZOOM_INCREMENT
          //if the newZoomFactor is set to lower than the min accepted
          //it won't change, and will never alert that it's at minimum
          newZoomFactor = newZoomFactor < MIN_ZOOM ? MIN_ZOOM : newZoomFactor
          browser.tabs.setZoom(tab.id, newZoomFactor)
        }
      })
    })
  }

  else if (e.target.id === "tabs-default-zoom") {
    callOnActiveTab((tab) => {
      let gettingZoom = browser.tabs.getZoom(tab.id)
      gettingZoom.then((zoomFactor) => {
        if (zoomFactor == DEFAULT_ZOOM) {
          alert("Tab zoom is already at the default zoom factor")
        } else {
          browser.tabs.setZoom(tab.id, DEFAULT_ZOOM)
        }
      })
    })
  }

  else if (e.target.classList.contains('switch-tabs')) {
    let tabId = +e.target.getAttribute('href')

    browser.tabs.query({
      currentWindow: true
    }).then((tabs) => {
      for (let tab of tabs) {
        if (tab.id === tabId) {
          browser.tabs.update(tabId, {
            active: true
          })
        }
      }
    })
  }

  else if (e.target.classList.contains('tab-preview')) {
    let tabId = +e.target.getAttribute('tabid')
    browser.tabs.get(tabId).then((tab) => {
      if (tab.discarded) {
        console.log(tab.id, " is discarded! - reloading!")
        browser.tabs.reload(tabId)
        e.target.classList.add('is-reloading')
      } else {
        console.log(tab.id, " ISN'T discarded!")
      }
    })
  }

  e.preventDefault()
})

//onRemoved listener. fired when tab is removed
browser.tabs.onRemoved.addListener((tabId, removeInfo) => {
  console.log(`The tab with id: ${tabId}, is closing`)

  if (removeInfo.isWindowClosing) {
    console.log(`Its window is also closing.`)
  } else {
    console.log(`Its window is not closing`)
  }
})

//onMoved listener. fired when tab is moved into the same window
browser.tabs.onMoved.addListener((tabId, moveInfo) => {
  let startIndex = moveInfo.fromIndex
  let endIndex = moveInfo.toIndex
  console.log(`Tab with id: ${tabId} moved from index: ${startIndex} to index: ${endIndex}`)
})

// onUpdated listener. fired when a tab is updated
browser.tabs.onUpdated.addListener(function (tabId, changeInfo) {
  // if the tab has finished loading
  if (changeInfo.status == "complete") {
    let tabPreview = document.getElementById(`tab-preview-${tabId}`)
    let isReloading = tabPreview.classList.contains('is-reloading')
    // if there is a tab preview for this tab and it's reloading
    if (tabPreview && isReloading) {
      // remove reloading class
      tabPreview.classList.remove('is-reloading')

      // console message
      let tabId = +tabPreview.getAttribute('tabid')
      console.log(tabId, " has finished (re)loading!")

      // recapture tab
      setTimeout(() => {
        browser.tabs.get(tabId).then((tab) => {
          updateTabPreview(tab, tabPreview)
        })
      }, 2000)
    }
  }
})
