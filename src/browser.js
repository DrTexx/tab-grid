export function getRecentlyClosedTabs() {
  return new Promise((resolve, reject) => {
    try {
      browser.sessions.getRecentlyClosed()
        .then((recentlyClosedStuff) => {
          let recentlyClosedTabs = recentlyClosedStuff.filter((recentlyClosed) => 'tab' in recentlyClosed)
          resolve(recentlyClosedTabs)
        })
    } catch (e) {
      reject(e)
    }
  })
}
