export function addImageUri(sessionId, imageUri) {
  let sessionCaptures = JSON.parse(sessionStorage.getItem('sessionCaptures'))

  if (!sessionCaptures) { sessionCaptures = {} }

  sessionCaptures[sessionId] = imageUri

  sessionStorage.setItem('sessionCaptures', JSON.stringify(sessionCaptures))
}

export function getImageUri(sessionId) {
  let sessionCaptures = JSON.parse(sessionStorage.getItem('sessionCaptures'))

  if (!sessionCaptures) { sessionCaptures = {} }

  let imageUri = sessionCaptures[sessionId]

  return imageUri || null
}

export function removeImageUri(sessionId) {
  let sessionCaptures = JSON.parse(sessionStorage.getItem('sessionCaptures'))

  if (!sessionCaptures) {
    console.log("notice: not removing session capture as it doesn't exist:", sessionId)
    return
  }

  delete sessionCaptures[sessionId]

  sessionStorage.setItem('sessionCaptures', JSON.stringify(sessionCaptures))
}
