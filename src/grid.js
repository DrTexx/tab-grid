import {
  createTabGridEntry,
  createSessionEntry,
  createRecentlyClosed,
  createTabGrid,
  updateTabPreview
} from "./dom"
import * as sessionCaptureStore from "./session-capture-store"
import { getRecentlyClosedTabs } from "./browser"

function listTabs() {
  createRecentlyClosed()
  createTabGrid()
}

function handleClick(e) {
  if (e.target.classList.contains('tab-preview')) {
    let tabId = +e.target.getAttribute('tabid')
    browser.tabs.get(tabId).then((tab) => {
      if (tab.discarded) {
        console.log(tab.id, " is discarded! - reloading!")
        browser.tabs.reload(tabId)
        e.target.classList.add('is-reloading')
      } else {
        console.log(tab.id, " ISN'T discarded! - reloading anyway!")
        browser.tabs.reload(tabId) // TODO(Denver): consider replacing this with just a recapture instead of reload
        e.target.classList.add('is-reloading')
        updateTabPreview(tab, e.target)
      }
    })
  }

  else if (e.target.classList.contains('tab-close')) {
    let tabId = +e.target.getAttribute('tabid')
    console.log(`closing tab!: ${tabId}`)
    browser.tabs.remove(tabId)
  }

  else if (e.target.classList.contains('session-entry')) {
    let sessionId = e.target.getAttribute('sessionId')

    // take note of the current tab
    browser.tabs.getCurrent().then((tab) => {
      // restore the clicked session
      browser.sessions.restore(sessionId)

      // navigate back to the original tab
      browser.tabs.update(tab.id, {
        active: true
      })
    })
  }

  e.preventDefault()
}

function handleDoubleClick(e) {
  if (e.target.classList.contains('tab-preview')) {
    let tabId = +e.target.getAttribute('tabid')
    browser.tabs.update(tabId, {
      active: true
    })
  }
}

function handleTabUpdated(tabId, changeInfo) {
  // if the tab has finished loading
  if (changeInfo.status == "complete") {
    let tabPreview = document.getElementById(`tab-preview-${tabId}`)
    let isReloading = tabPreview.classList.contains('is-reloading')
    // if there is a tab preview for this tab and it's reloading
    if (tabPreview && isReloading) {
      // console message
      let tabId = +tabPreview.getAttribute('tabid')
      console.log(tabId, " has finished (re)loading!")

      // recapture tab
      setTimeout(() => {
        browser.tabs.get(tabId).then((tab) => {
          // remove reloading class
          tabPreview.classList.remove('is-reloading')

          updateTabPreview(tab, tabPreview)
        })
      }, 2000)
    }
  }
}

// HACK(Denver): There's no direct connection between sessionId and tabId,
// instead trying to use something else that mightn't necessarily be unique or accurate
function _getLastTabClosed() {
  return new Promise((resolve, reject) => {
    try {
      browser.sessions.getRecentlyClosed().then((recentlyClosedStuff) => {
        let recentlyClosedTab = recentlyClosedStuff
          .find((recentlyClosed) => {
            if ('tab' in recentlyClosed) { return recentlyClosed.tab }
          })

        resolve(recentlyClosedTab)
      })
    } catch (e) {
      reject(e)
    }
  })
}

function handleTabRemoved(tabId) {
  let tabGridEntry = document.getElementById(`tag-grid-entry-${tabId}`)
  let imageUri = document.getElementById(`tab-preview-${tabId}`).src

  // if there's a tab grid entry matching this tab id
  if (tabGridEntry) {
    // remove the tab grid entry
    tabGridEntry.remove()
  }

  // store the tab capture image uri alongside the closed tab's session id
  _getLastTabClosed()
    .then((lastTabClosed) => {
      let sessionId = lastTabClosed.tab.sessionId
      sessionCaptureStore.addImageUri(sessionId, imageUri)
    })
}

function handleSessionsChanged() {
  getRecentlyClosedTabs()
    .then((recentlyClosedTabs) => {
      let sessionIds = []

      recentlyClosedTabs.forEach((session) => {
        // we need to get an array of session ids anyway so we might as well
        // collect them here as we're iterating through all the sessions
        sessionIds.push(session.tab.sessionId)

        let sessionEntry = document.getElementById(`session-entry-${session.tab.sessionId}`)
        // if there is not a session entry for this session
        if (!sessionEntry) {
          // create and prepend new session entry
          let recentlyClosed = document.getElementById('recently-closed')

          let sessionEntry = createSessionEntry(session)

          console.log("adding this session entry:", sessionEntry)
          recentlyClosed.prepend(sessionEntry)
        }
      })

      // for every session entry
      document.querySelectorAll('.session-entry')
        .forEach((sessionEntry) => {
          let sessionId = sessionEntry.getAttribute('sessionid')

          // if session entry has a session id that isn't in the actual session ids
          if (!(sessionIds.includes(sessionId))) {
            console.log("removing this session entry:", sessionEntry)

            sessionEntry.remove()

            // remove imageUri from sessionCaptures
            sessionCaptureStore.removeImageUri(sessionId)
          }
        })

      // console.log("session entry elements:", document.querySelectorAll('.session-entry').length)
    })
}

function handleTabCreated(tab) {
  let tabGrid = document.getElementById('tab-grid')

  let tabGridEntry = createTabGridEntry(tab)

  tabGrid.prepend(tabGridEntry)

  let tabPreview = document.getElementById(`tab-preview-${tab.id}`)
  console.log({ tabPreview })
  setTimeout(() => {
    updateTabPreview(tab, tabPreview)
  }, 2000)
}

document.addEventListener("DOMContentLoaded", listTabs)

document.addEventListener("click", handleClick)

// TODO(Denver): replace trigger with a button - double-click isn't ideal for mobile support
document.addEventListener('dblclick', handleDoubleClick)

// onUpdated listener. fired when a tab is updated
browser.tabs.onUpdated.addListener(handleTabUpdated)

//onRemoved listener. fired when tab is removed
browser.tabs.onRemoved.addListener(handleTabRemoved)

browser.sessions.onChanged.addListener(handleSessionsChanged)

browser.tabs.onCreated.addListener(handleTabCreated)
