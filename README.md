# Tab Grid

An extension for lovely people that want to clean up their tabs :)

[![Mozilla Add-on](https://img.shields.io/amo/v/tab-grid?label=Firefox%20Extension&logo=Firefox)](https://addons.mozilla.org/en-US/firefox/addon/tab-grid/)
[![GitLab](https://img.shields.io/gitlab/license/DrTexx/tab-grid?color=green)](https://gitlab.com/DrTexx/tab-grid/-/blob/main/LICENSE)

## Installation

[![GET THE ADD-ON (FIREFOX)](https://user-images.githubusercontent.com/585534/107280546-7b9b2a00-6a26-11eb-8f9f-f95932f4bfec.png)](https://addons.mozilla.org/en-US/firefox/addon/tab-grid/)

## User stories

- [x] As a user, I want to be able to preview the contents of all my tabs from a grid view
- [x] As a user, I want to be able to close tabs easily after previewing them
- [x] As a user, I want to be able to undo accidentally closing any tabs
- [ ] As a user, I want to be able to identify tabs quickly
  - [ ] Page icons (favicons)
  - [ ] Page names (titles)
- [ ] As a user, I want to be able to limit the number of previews in the tab grid
  - [ ] As a user, I want to be able to  increase/decrease this limit
- [ ] As a user, I want to be able to group together tabs based on the domain of the website they're pointing to
- [ ] As a user, I want to be able to sort tabs by when I opened them
- [ ] As a user, I want to be able to sort tabs by when I last interacted with them
- [ ] As a user, I want to be able to group together tabs opened within a short period of each other so that I can easily identify any rabbit-holes I went down
- [ ] As a user, I want to know what types of tabs I'm frequently closing and get relevant suggestions for others I might want to close
	- *example: If I'm often closing gmail tabs, I want Tab Grid to suggest it by saying something like "You often close these kinds of tabs, do you want to close any of these ones?"*

## Who is this useful for?

- People who go down rabbit-holes
- People who don't like bookmarks as much as they do tabs
- People that organize things visually
- People with ADD/ADHD
- People curious about writing their own tab management extensions
- People helping others clean up their tabs

## Any suggestions?

Please [create a new GitLab issue](https://gitlab.com/DrTexx/tab-grid/-/issues/new) if you have any wonderful ideas!

## Any questions?

You can direct message me on [Twitter](https://twitter.com/DrTexx) for any inquiries, perplexments or curiosities 🌱

## Development

**Requirements**
| Requirement                                   | Version         | Notes |
| --------------------------------------------- | --------------- | ----- |
| [NodeJS](https://nodejs.org/en/)              | *latest LTS*    | -     |
| [web-ext](https://github.com/mozilla/web-ext) | *latest stable* | -     |


```bash
git clone https://gitlab.com/DrTexx/tab-grid  # clone repository
cd tab-grid
npm install
npm run dev  # automatically rebuild extension
```

### Building

```bash
./build.sh
```

## Permissions

- **Tabs** - to manage tabs and capture thumbnails
- **All URLs** - to allow Tab Grid to manage any tab
- **Sessions** - to display information related to recently closed tabs
- **Storage** - to store recently closed tab thumbnails in memory

## Privacy

While Tab Grid is running,
- Tab Grid will **never collect or sell your personal information**
- **Tab information will be stored temporarily** in memory and retrieved using your browser's extension APIs
- **No information will be written to disk** by Tab Grid itself, however we can't guarantee your browser won't cache or store information Tab Grid requires to function
